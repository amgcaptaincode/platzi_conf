package com.amg.platziconf.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.amg.platziconf.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}