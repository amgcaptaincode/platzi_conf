package com.amg.platziconf.model

import java.util.*

class Conference {

    lateinit var datetime: Date
    lateinit var description: String
    lateinit var speaker: String
    lateinit var tag: String
    lateinit var title: String

}