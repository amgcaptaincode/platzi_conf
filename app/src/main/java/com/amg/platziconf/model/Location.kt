package com.amg.platziconf.model

class Location {
    val name = ""
    val address = ""
    val latitude = ""
    val longitude = ""
    val phone = ""
    val website = ""
    val photo = ""
}